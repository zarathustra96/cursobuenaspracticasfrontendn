import * as axios from "axios"
import { urlApi } from "./urlAPI"


export const settings = () => {
    return {
        headers :{
            "Content-Type": "application/json",
        }
    }
}

export const services = (method,service,body) => {

    switch (method) {
        case "GET":
      
            let url  = `${urlApi}${service}`;
            return axios.get(url,config()).then((r)=>{
                return response(r)
            }).catch((err)=>{
                return response(err.response);
            });

           
        case "POST":

            return axios.post(`${urlApi}${service}`,body,config()).then((r)=>{
                return response(r);
            }).catch((err)=>{
                return response(err.response);
            });

           
        case "PUT":
            return axios.put(`${urlApi}${service}`,body,config()).then((r)=>{
                return response(r);
            }).catch((err)=>{
                return response(err.response);
            });
            
        case "DELETE":
            return axios.delete(`${urlApi}${service}`,config()).then((r)=>{
                return response(r);
            }).catch((err)=>{
                return  response(err.response);
            });

        default:
            break;
    }
}

const response = (r) => {
    
    if(r === undefined){
        return false;
    }

    if(r.status === 401){
        window.localStorage.clear();
        window.location.replace('/');
        return false
    }

    if(r.status === 200 || r.status === 201){
        return {status:r.status, data:r.data}
    }
    return {status:r.status, errors: r.data.error}
} 